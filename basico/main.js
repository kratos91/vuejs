const app=Vue.createApp({
    data(){
        return{
            titulo:'Este es un titulo con vuejs',
            cantidad:0,
            enlace:'https://www.google.com',
            estado:false,
            servicios:['transeferencias','depositos','retiros','cheques'],
            desactivar:false
        }
    },
    methods: {
        agregarSaldo(){
            this.cantidad=this.cantidad+100
        },
        disminuirSaldo(valor){
            if (this.cantidad===0) {
                alert('Saldo en 0')
                this.desactivar=true
                return
            }
            this.cantidad=this.cantidad-valor
        }     
    },computed: {
        colorCantidad(){
            return this.cantidad > 500 ? 'text-success' : 'text-danger';
        },
        mayusculasTexto(){
            return this.titulo.toUpperCase()
        } 
    }
})